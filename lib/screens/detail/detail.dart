import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_shopping_app/screens/detail/widgets/add_cart.dart';
import 'package:my_shopping_app/screens/detail/widgets/detail_app_bar.dart';
import 'package:my_shopping_app/screens/detail/widgets/item_info.dart';

import '../../models/clothes.dart';

class DetailPage extends StatelessWidget {
  final Clothes clothes;
  DetailPage(this.clothes);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment:CrossAxisAlignment.start,
          children: [
            DetailAppBar(clothes),
            ItemInfo(clothes),
            AddCart(clothes)
          ],
        ),
      ),
    );
  }
}
