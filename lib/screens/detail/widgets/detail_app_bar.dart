import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_shopping_app/models/clothes.dart';

class DetailAppBar extends StatefulWidget {
  late final Clothes clothes;

  DetailAppBar(this.clothes);

  @override
  _DetailAppBarState createState() => _DetailAppBarState();
}

class _DetailAppBarState extends State<DetailAppBar> {
  final CarouselController _controller = CarouselController();
  int _current_page = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Container(
              child: CarouselSlider(
            carouselController: _controller,
            options: CarouselOptions(
                height: 400,
                viewportFraction: 1,
                onPageChanged: (index, reason) {
                  setState(() {
                    _current_page = index;
                  });
                }),
            items: widget.clothes.detailUrl
                .map((e) => Builder(
                    builder: (context) => Container(
                          margin: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage('$e'),
                                fit: BoxFit.fitHeight,
                              ),
                              borderRadius: BorderRadius.circular(25)),
                        )))
                .toList(),
          )),
          Positioned(
              bottom: 30,
              left: 180,
              child: Row(
                  children: widget.clothes.detailUrl
                      .asMap()
                      .entries
                      .map((e) => GestureDetector(
                              onTap: (){
                                _controller.animateToPage(e.key);
                              },
                              child: Container(
                                width: 12,
                                height: 12,
                                margin: EdgeInsets.symmetric(horizontal: 4),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white.withOpacity(
                                        _current_page == e.key ? 0.9 : 0.4)),
                          )))
                      .toList())
          ),
          Container(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top+25,
                left: 25,
                right: 25
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.grey,
                          size: 20,
                        ),
                    ),
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white.withOpacity(0.9)
                    ),
                  ),
                ),
                Container(
                  child: Icon(
                    Icons.more_horiz,
                    color: Colors.grey,
                    size: 20,
                  ),
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white.withOpacity(0.9)
                  ),
                )
              ],
            ),

          )
        ],
      ),
    );
  }
}
