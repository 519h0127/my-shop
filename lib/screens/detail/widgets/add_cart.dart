import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_shopping_app/models/clothes.dart';

class AddCart extends StatefulWidget {
  late final Clothes clothes;
  AddCart(this.clothes);
  @override
  _AddCartState createState() => _AddCartState();
}

class _AddCartState extends State<AddCart> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(25),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Price",
                style: TextStyle(
                    fontSize: 16,color: Colors.grey
                 ),
              ),
              Row(
                children: [
                  Text('\$ ${widget.clothes.price}',style: TextStyle(
                      height: 1.5,
                      fontSize: 20,
                      fontWeight: FontWeight.bold
                  )),
                  SizedBox(width: 6,),
                  Text('\$ ${double.parse(widget.clothes.price)+20}', style: TextStyle(height: 1.5,decoration: TextDecoration.lineThrough)),
                ],
              )
            ],
          ),
          Container(
            height: 50,
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)
                  ),
                  elevation: 0,
                  primary: Theme.of(context).primaryColor
                ),
                onPressed: (){},
                child: Row(
                  children: [
                    Text('Add to Cart'),
                    Icon(Icons.shopping_cart_outlined)
                  ],
                )),
          )
        ],
      ),
    );
  }
}
