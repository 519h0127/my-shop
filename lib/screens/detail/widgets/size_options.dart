import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SizeOption extends StatefulWidget {

  @override
  _SizeOptionState createState() => _SizeOptionState();
}

class _SizeOptionState extends State<SizeOption> {
  final List<String> sizeList = ['S','M','L','XL','XXL'];
  var currentSelected = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      padding: EdgeInsets.symmetric(horizontal: 0,vertical: 10),
      child: ListView.separated(
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) => GestureDetector(
            onTap: (){
               setState(() {
                 currentSelected = index;
               });
            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: currentSelected == index
                    ?
                        Theme.of(context).primaryColor.withOpacity(0.3)
                    :
                        Colors.white
              ),
              child: Text(
                sizeList[index],
                style: TextStyle(
                  color: currentSelected == index
                      ?
                        Theme.of(context).primaryColor
                      :
                        Colors.grey,
                  fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          separatorBuilder: (_,index)=>SizedBox(width: 10,),
          itemCount: sizeList.length),
    );
  }
}
