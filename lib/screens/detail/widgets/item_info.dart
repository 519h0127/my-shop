import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_shopping_app/screens/detail/widgets/size_options.dart';

import '../../../models/clothes.dart';

class ItemInfo extends StatelessWidget {
  final Clothes clothes;
  ItemInfo(this.clothes);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 25,vertical: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment:  MainAxisAlignment.spaceBetween,
              children: [
                Text('${clothes.title} ${clothes.subtitle}',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
                Icon(Icons.favorite,color: Colors.red,)


              ],
            ),
            Row(
              children: [
                Icon(
                  Icons.star_border,
                  color: Theme.of(context).primaryColor,
                ),
                Text(
                  '4.5 (2.7k)',
                  style:
                    TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.grey),

                )
              ],
            ),
            SizedBox(height: 10,),
            RichText(text: TextSpan(
              children: [
                TextSpan(style: TextStyle(color: Colors.grey.withOpacity(0.7)),text: "The fascination with the past continues to play a key role in shaping the House's narrative. Souvenir shirts that look as if they could be found in LA’s vintage markets are reworked into the collection. Here, this bowling shirt is crafted from ivory and light blue viscose check."),
                TextSpan(text: 'Read More',style: TextStyle(
                  color: Theme.of(context).primaryColor
                )),
              ]
            )),
            SizeOption()
          ],
        ),
    );
  }
}
