import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_shopping_app/controller/clothes_controller.dart';
import 'package:my_shopping_app/screens/home/widgets/categories_list.dart';
import 'package:my_shopping_app/screens/home/widgets/clothes_item.dart';
import 'package:my_shopping_app/screens/home/widgets/custom_app_bar.dart';
import 'package:my_shopping_app/screens/home/widgets/new_arrival.dart';
import 'package:my_shopping_app/screens/home/widgets/search_input.dart';

import '../../models/clothes.dart';

class HomePage extends StatelessWidget{
  // List<Clothes> clothesList = Clothes.newArrivalList();


  final bottomTags = ['home','menu','heart','user'];
  int _index = 0;
  @override
  Widget build(BuildContext context) {

      switch(_index) {
        case 0:
          print(bottomTags[_index]);
          break;
        case 1:
          print(bottomTags[_index]);
          break;
        case 2:
          print(bottomTags[_index]);
          break;
        case 3:
          print(bottomTags[_index]);
          break;
      }
      return Scaffold(
          body: FutureBuilder(
            future: ClothesController.getClothes(),
            builder: (context,AsyncSnapshot<List<Clothes>> snapshot) {
              print(snapshot.data);
              return SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children:  [
                    CustomeAppBar(),
                    SearchInput(),
                    NewArrival("New Items",snapshot.data),
                    NewArrival("Men",snapshot!.data),
                  ],
                ),
              );
            }
          ),
          bottomNavigationBar: BottomNavigationBar(
            onTap: (int index){_index = index;},
            showSelectedLabels: false,
            showUnselectedLabels: false,
            type: BottomNavigationBarType.fixed,
            items: bottomTags.map((e) => BottomNavigationBarItem(
              label: e,icon: Image.asset('assets/icons/$e.png',width: 25,)
            )).toList()

          ),
      );
  }
}