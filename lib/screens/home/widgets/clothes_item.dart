import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../models/clothes.dart';
import '../../detail/detail.dart';

class ClothesItem extends StatelessWidget {
  late final Clothes clothes;
  ClothesItem(this.clothes);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      child:GestureDetector(
        onTap: (){
          Navigator.of(context).push(
            MaterialPageRoute(builder: (context)=>DetailPage(clothes))
          );
        },
        child: Card(
          shape:RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15)
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Stack(
                children: [
                  Container(
                    height: 170,
                    width: 200,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      image: DecorationImage(
                        image: AssetImage(clothes.imageUrl),
                        fit: BoxFit.fitHeight
                      ),
                    ),
                  ),
                  Positioned(
                    right: 15,
                    top: 10,
                    child: Icon(Icons.favorite,color: Colors.red,)),
                ],
              ),
              SizedBox(height: 10,),
              Text(clothes.title,style: TextStyle(
                fontWeight: FontWeight.bold,
                height: 1.5,
              ),),
              SizedBox(height: 5,),
              Text(clothes.subtitle,style: TextStyle(
                fontWeight: FontWeight.bold,
                height: 1.5,
              )),
              Text('\$${clothes.price}',style: TextStyle(
                fontWeight: FontWeight.bold,
                height: 2,
                color: Theme.of(context).primaryColor
              ))
            ],
          ),
        ),
      ),
    );
  }
}
