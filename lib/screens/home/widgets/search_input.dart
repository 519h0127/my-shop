import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class  SearchInput extends StatelessWidget {
  final tags = ['Woman','T-Shirt','Dress'];

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 25,left: 25,right: 25),
      child: Column(
        children: [
          Row(
            children: [
              Flexible(
                  flex: 1,
                  child:  TextField(
                    cursorColor: Colors.grey,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.grey.withOpacity(0.3),
                      border: OutlineInputBorder(
                         borderSide: BorderSide.none,
                         borderRadius:  BorderRadius.all(Radius.circular(10)),
                      ),
                      hintText: "Search Asthetic Shirt ",
                      hintStyle: TextStyle(
                        color: Colors.grey,
                        fontSize: 18,
                      ),
                      prefixIcon: Container(
                          padding: EdgeInsets.all(15),
                          child: Image.asset('assets/icons/search.png',width: 20,),
                      ),
                    ),
                  )
              ),
              Container(
                margin: EdgeInsets.only(left: 10),
                padding:EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color:Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Image.asset('assets/icons/filter.png',width: 25),
              )
            ],
          ),
          Row(
            children:
              tags.map((e) => Container(
                margin: EdgeInsets.only(top: 10,right: 10),
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.deepOrangeAccent
                ),
                child: Text(e,style: TextStyle(
                      color: Colors.white,
                      fontWeight:FontWeight.bold,
                  ),
                )
              )).toList(),
          ),
        ],
      ),
    );

  }
}
