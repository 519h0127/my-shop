import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CategoriesList extends StatelessWidget {
  late final String title;
  CategoriesList(this.title);
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 25,vertical: 20),
        child:Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(title,style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 17,
            ),),
            Row(
              children: [
                Text('View All',style: TextStyle(
                  color:Theme.of(context).primaryColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 15
                ),),
                SizedBox(width: 8,),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Theme.of(context).primaryColor
                  ),
                  child: Icon(Icons.arrow_forward_ios,color: Colors.white,size: 14,),
                )
              ],
            )
          ],
        ),
    );
  }
}
