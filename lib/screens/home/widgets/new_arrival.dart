import 'package:flutter/cupertino.dart';
import 'package:my_shopping_app/screens/home/widgets/categories_list.dart';
import 'package:my_shopping_app/screens/home/widgets/clothes_item.dart';

import '../../../models/clothes.dart';

class  NewArrival extends StatelessWidget {
  late final String title;
  List<Clothes>? clothesList;

  NewArrival(this.title, this.clothesList, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          CategoriesList(title),
          Container(
            height: 280,
            margin: EdgeInsets.only(left: 15),
            child: ListView.separated(
                scrollDirection: Axis.horizontal,
                itemBuilder: (context,index)=>
                    ClothesItem(clothesList![index]),
                separatorBuilder: (_,index)=>SizedBox(width: 10,),
                itemCount: clothesList!.length),
          )
        ],
      ),
    );
  }
}
