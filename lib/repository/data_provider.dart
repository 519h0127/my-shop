import 'package:flutter/material.dart';
import '../controller/clothes_controller.dart';

class DataProvider extends InheritedWidget {
  final _controller = ClothesController();

  DataProvider({Key? key, required Widget child}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(covariant InheritedWidget oldWidget) => false;

  static ClothesController of(BuildContext context) {
    DataProvider provider = context.dependOnInheritedWidgetOfExactType<DataProvider>() as DataProvider;

    return provider._controller;
  }

}