

import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:my_shopping_app/models/clothes.dart';

class ClothesService{
  Future<List<Clothes>> getClothesList() async {
    final response = await rootBundle.loadString('assets/data.json');
    final data = json.decode(response);
    // print(data[1]);
    List<Clothes> clothes = [];
    for(int i = 0;i< data.length;i++){
      List<String> detailUrl = [];
      for(int j = 0;j < data[i]['detailUrl'].length;j++){
        detailUrl.add(data[i]['detailUrl'][j]);
      }

      Clothes clothe = new Clothes(
          title: "${data[i]['title']}",
          subtitle: "${data[i]['subtitle']}",
          price: '${data[i]['price']}',
          imageUrl: "${data[i]['imageUrl']}",
          detailUrl: detailUrl);
      // Clothes clothe = new Clothes.fromJson(data[i]);
      clothes.add(clothe);
    }
    print(clothes);
    return clothes;
  }
}