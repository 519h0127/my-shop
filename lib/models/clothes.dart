const keyTitle = 'title';
const keySubtitle = 'subtitle';
const keyPrice = 'price';
const keyImageUrl = 'imageUrl';
const keyDetailUrl = 'detailUrl';

class Clothes{
  late String title;
  late String subtitle;
  late String price;
  late String imageUrl;
  late List<String> detailUrl = [];

  Clothes({required this.title,required this.subtitle,required this.price,required this.imageUrl,required this.detailUrl});

  Clothes.createDefault() {
    title = "";
    subtitle = '';
    price = '';
    imageUrl = '';
    detailUrl = [];
  }


  Clothes.fromJson(Map<String, dynamic> json) {
    // print(json['detailUrl'].toString());
    title = json['title'] ?? '';
    subtitle = json['subtitle'] ?? '';
    price = json['price'] ?? '';
    imageUrl = json['imageUrl'] ?? '';
    detailUrl = json['detailUrl'] ?? '';
  }

  Map<String, dynamic> toJson() {
    return {
      keyTitle: title,
      keySubtitle: subtitle,
      keyPrice: price,
      keyImageUrl: imageUrl,
      keyDetailUrl: detailUrl,
    };
  }

}