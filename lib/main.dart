import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_shopping_app/repository/data_provider.dart';
import 'screens/home/homePage.dart';


void main() {
  runApp(DataProvider(child: MyApp()));
}

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.transparent
      ));
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          backgroundColor: Color(0xffffafafa),
          primaryColor: Color(0xffffbd00), colorScheme: ColorScheme.fromSwatch().copyWith(secondary: Color(0xffffbc7))
        ),
        home:HomePage(),
      );
  }
}

