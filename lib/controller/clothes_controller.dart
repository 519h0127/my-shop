import 'package:my_shopping_app/models/clothes.dart';

import '../service/clothes_service.dart';

class ClothesController {
  static Future<List<Clothes>> getClothes() async {
    print('Get list of clothes...');
    ClothesService helper = ClothesService();
    List<Clothes> clothes = await helper.getClothesList();
    print(clothes);
    return clothes;
  }
}